#!/bin/bash

sudo apt update
sudo apt upgrade -y

curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt install gitlab-runner
sudo usermod -aG docker gitlab-runner

sudo gitlab-runner register \
--non-interactive \
--url https://gitlab.com/ \
--registration-token GR13489411QXVbxmZSfmUYU4gxyo7 \
--executor shell \
--tag-list "VAGRANT,DOCKER"